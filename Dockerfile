FROM rust:latest
RUN rustup target add x86_64-pc-windows-gnu; \
    apt update; \
    apt install -y clang gcc-mingw-w64-x86-64 g++-mingw-w64-x86-64 wine64; \
    apt clean

ENV CARGO_BUILD_TARGET=x86_64-pc-windows-gnu
